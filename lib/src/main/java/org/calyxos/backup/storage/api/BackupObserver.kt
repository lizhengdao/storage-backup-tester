package org.calyxos.backup.storage.api

public interface BackupObserver {

    public suspend fun onStart()

    /**
     * Called after scanning files when starting the backup.
     *
     * @param totalSize the total size of all files to be backed up.
     * @param numFiles the number of all files to be backed up.
     * The sum of [numSmallFiles] and [numLargeFiles].
     * @param numSmallFiles the number of small files to be backed up.
     * @param numLargeFiles the number of large files to be backed up.
     */
    public suspend fun onBackupStart(
        totalSize: Long,
        numFiles: Int,
        numSmallFiles: Int,
        numLargeFiles: Int,
    )

    public suspend fun onFileBackedUp(
        file: BackupFile,
        wasUploaded: Boolean,
        reusedChunks: Int,
        bytesWritten: Long,
        tag: String,
    )

    public suspend fun onFileBackupError(
        file: BackupFile,
        tag: String,
    )

    public suspend fun onBackupComplete(backupDuration: Long)

}
