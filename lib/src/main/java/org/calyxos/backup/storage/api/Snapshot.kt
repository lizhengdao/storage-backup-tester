package org.calyxos.backup.storage.api

import org.calyxos.backup.storage.backup.BackupSnapshot

public data class SnapshotItem(
    public val time: Long,
    public val snapshot: BackupSnapshot?,
)

public sealed class SnapshotResult {
    public data class Success(val snapshots: List<SnapshotItem>) : SnapshotResult()
    public data class Error(val e: Exception) : SnapshotResult()
}
