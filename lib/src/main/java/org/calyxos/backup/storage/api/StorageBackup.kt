package org.calyxos.backup.storage.api

import android.content.Context
import android.net.Uri
import android.provider.DocumentsContract.isTreeUri
import android.provider.MediaStore
import android.util.Log
import androidx.annotation.WorkerThread
import androidx.room.Room
import kotlinx.coroutines.Dispatchers
import kotlinx.coroutines.flow.Flow
import kotlinx.coroutines.withContext
import org.calyxos.backup.storage.backup.Backup
import org.calyxos.backup.storage.backup.BackupSnapshot
import org.calyxos.backup.storage.db.Db
import org.calyxos.backup.storage.restore.FileRestore
import org.calyxos.backup.storage.restore.Restore
import org.calyxos.backup.storage.scanner.DocumentScanner
import org.calyxos.backup.storage.scanner.FileScanner
import org.calyxos.backup.storage.scanner.MediaScanner
import org.calyxos.backup.storage.toStoredUri

private const val TAG = "StorageBackup"

@Suppress("BlockingMethodInNonBlockingContext")
public class StorageBackup(
    private val context: Context,
    plugin: StoragePlugin,
) {

    private val db: Db by lazy {
        Room.databaseBuilder(context, Db::class.java, "local-cache")
            .fallbackToDestructiveMigration() // TODO remove before release
            .build()
    }
    private val uriStore by lazy { db.getUriStore() }

    private val mediaScanner by lazy { MediaScanner(context) }
    private val backup by lazy {
        val documentScanner = DocumentScanner(context)
        val fileScanner = FileScanner(uriStore, mediaScanner, documentScanner)
        Backup(context, db, fileScanner, plugin)
    }
    private val restore by lazy { Restore(context, plugin, FileRestore(context, mediaScanner)) }

    public val uris: Set<Uri>
        @WorkerThread
        get() {
            return uriStore.getStoredUris().map { it.uri }.toSet()
        }

    @Throws(IllegalArgumentException::class)
    public suspend fun addUri(uri: Uri): Unit = withContext(Dispatchers.IO) {
        if (uri.authority == MediaStore.AUTHORITY) {
            if (uri !in mediaUris) throw IllegalArgumentException("Not a supported MediaStore URI")
        } else if (uri.authority == EXTERNAL_STORAGE_PROVIDER_AUTHORITY) {
            if (!isTreeUri(uri)) throw IllegalArgumentException("Not a tree URI")
        } else {
            throw IllegalArgumentException()
        }
        Log.e(TAG, "Adding URI $uri")
        uriStore.addStoredUri(uri.toStoredUri())
    }

    public suspend fun removeUri(uri: Uri): Unit = withContext(Dispatchers.IO) {
        Log.e(TAG, "Removing URI $uri")
        uriStore.removeStoredUri(uri.toStoredUri())
    }

    @Deprecated("TODO remove for release")
    public fun clearCache() {
        db.clearAllTables()
    }

    public suspend fun runBackup(backupObserver: BackupObserver?): Boolean {
        return try {
            backup.runBackup(backupObserver)
            true
        } catch (e: Exception) {
            Log.e(TAG, "Error during backup", e)
            false
        }
    }

    public fun getBackupSnapshots(): Flow<SnapshotResult> {
        return restore.getBackupSnapshots()
    }

    public suspend fun restoreBackupSnapshot(
        snapshot: BackupSnapshot,
        restoreObserver: RestoreObserver? = null
    ): Boolean {
        return try {
            restore.restoreBackupSnapshot(snapshot, restoreObserver)
            true
        } catch (e: Exception) {
            Log.e(TAG, "Error during restore", e)
            false
        }
    }

}
