package org.calyxos.backup.storage.db

import androidx.room.ColumnInfo
import androidx.room.Dao
import androidx.room.Delete
import androidx.room.Entity
import androidx.room.Insert
import androidx.room.PrimaryKey
import androidx.room.Query

@Entity
internal data class CachedChunk(
    @PrimaryKey val id: String,
    @ColumnInfo(name = "ref_count") val refCount: Long,
    val size: Long
)

@Dao
internal interface ChunksCache {
    @Insert
    fun insert(chunk: CachedChunk)

    @Query("SELECT * FROM CachedChunk WHERE id == (:id)")
    fun get(id: String): CachedChunk?

    @Query("SELECT * FROM CachedChunk WHERE ref_count <= 0")
    fun getUnreferencedChunks(): List<CachedChunk>

    @Query("UPDATE CachedChunk SET ref_count = ref_count + 1 WHERE id IN (:ids)")
    fun incrementRefCount(ids: Collection<String>)

    @Query("UPDATE CachedChunk SET ref_count = ref_count - 1 WHERE id IN (:ids)")
    fun decrementRefCount(ids: List<String>)

    @Delete
    fun deleteChunks(chunks: List<CachedChunk>)
}
