package org.calyxos.backup.storage.backup

import android.Manifest.permission.ACCESS_MEDIA_LOCATION
import android.content.Context
import android.content.pm.PackageManager.PERMISSION_GRANTED
import android.os.Build
import android.text.format.Formatter
import android.util.Log
import org.calyxos.backup.storage.api.BackupObserver
import org.calyxos.backup.storage.api.StoragePlugin
import org.calyxos.backup.storage.crypto.ChunkCrypto
import org.calyxos.backup.storage.crypto.StreamCrypto
import org.calyxos.backup.storage.db.Db
import org.calyxos.backup.storage.scanner.FileScanner
import org.calyxos.backup.storage.scanner.FileScannerResult
import java.io.IOException
import kotlin.time.ExperimentalTime
import kotlin.time.measureTime
import kotlin.time.measureTimedValue

internal class BackupResult(
    val chunkIds: Set<String>,
    val backupMediaFiles: List<BackupMediaFile>,
    val backupDocumentFiles: List<BackupDocumentFile>,
) {
    operator fun plus(other: BackupResult) = BackupResult(
        chunkIds = chunkIds + other.chunkIds,
        backupMediaFiles = backupMediaFiles + other.backupMediaFiles,
        backupDocumentFiles = backupDocumentFiles + other.backupDocumentFiles,
    )
}

@Suppress("BlockingMethodInNonBlockingContext")
internal class Backup(
    private val context: Context,
    private val db: Db,
    private val fileScanner: FileScanner,
    private val storagePlugin: StoragePlugin,
    chunkSizeMax: Int = CHUNK_SIZE_MAX,
    private val streamCrypto: StreamCrypto = StreamCrypto,
) {

    companion object {
        const val VERSION: Byte = 0
        const val CHUNK_SIZE_MAX = 15 * 1024 * 1024
        const val SMALL_FILE_SIZE_MAX = 2 * 1024 * 1024
        private const val TAG = "Backup"
    }

    private val contentResolver = context.contentResolver
    private val filesCache = db.getFilesCache()
    private val chunksCache = db.getChunksCache()

    private val mac = ChunkCrypto.getMac(ChunkCrypto.deriveChunkIdKey(storagePlugin.getMasterKey()))
    private val streamKey = streamCrypto.deriveStreamKey(storagePlugin.getMasterKey())
    private val chunkWriter = ChunkWriter(streamCrypto, streamKey, chunksCache, storagePlugin)
    private val hasMediaAccessPerm =
        context.checkSelfPermission(ACCESS_MEDIA_LOCATION) == PERMISSION_GRANTED
    private val fileBackup = FileBackup(
        contentResolver = contentResolver,
        hasMediaAccessPerm = hasMediaAccessPerm,
        filesCache = filesCache,
        chunker = Chunker(mac, chunkSizeMax),
        chunkWriter = chunkWriter,
    )
    private val smallFileBackup = SmallFileBackup(
        contentResolver = context.contentResolver,
        hasMediaAccessPerm = hasMediaAccessPerm,
        filesCache = filesCache,
        zipChunker = ZipChunker(mac, chunkWriter),
    )

    @Throws(IOException::class)
    @OptIn(ExperimentalTime::class)
    suspend fun runBackup(backupObserver: BackupObserver?) {
        backupObserver?.onStart()

        // TODO check those chunks against cache
        storagePlugin.getAvailableChunkIds()

        val timedScanResult = measureTimedValue {
            fileScanner.getFiles()
        }
        Log.e(TAG, "Scanning took ${timedScanResult.duration}")

        val scanResult = timedScanResult.value
        val smallFiles = scanResult.smallFiles
        val largeFiles = scanResult.files
        val totalFiles = smallFiles.size + largeFiles.size
        val totalSize = smallFiles.sumOf { it.size } + largeFiles.sumOf { it.size }
        backupObserver?.onBackupStart(totalSize, totalFiles, smallFiles.size, largeFiles.size)

        val duration = measureTime {
            backupFiles(scanResult, backupObserver)
        }
        Log.e(TAG, "Changed files backup took $duration")
        backupObserver?.onBackupComplete(duration.toLongMilliseconds())
    }

    @OptIn(ExperimentalTime::class)
    @Throws(IOException::class)
    private suspend fun backupFiles(
        filesResult: FileScannerResult,
        backupObserver: BackupObserver?
    ) {
        val startTime = System.currentTimeMillis()
        val timedSmallResult = measureTimedValue {
            smallFileBackup.backupFiles(filesResult.smallFiles, backupObserver)
        }
        val smallTotal =
            timedSmallResult.value.backupMediaFiles.size + timedSmallResult.value.backupDocumentFiles.size
        Log.e(TAG, "Backing up $smallTotal small files took ${timedSmallResult.duration}")
        val timedResult = measureTimedValue {
            fileBackup.backupFiles(filesResult.files, backupObserver)
        }
        val largeTotal =
            timedResult.value.backupMediaFiles.size + timedResult.value.backupDocumentFiles.size
        Log.e(TAG, "Backing up $largeTotal files took ${timedResult.duration}")

        val result = timedResult.value + timedSmallResult.value
        val backupSize = result.backupMediaFiles.sumOf { it.size } +
                result.backupDocumentFiles.sumOf { it.size }
        val endTime = System.currentTimeMillis()

        val backupSnapshot: BackupSnapshot
        val snapshotWriteTime = measureTime {
            backupSnapshot = BackupSnapshot.newBuilder()
                .setVersion(VERSION.toInt())
                .setName("Backup on ${Build.MANUFACTURER} ${Build.MODEL}")
                .addAllMediaFiles(result.backupMediaFiles)
                .addAllDocumentFiles(result.backupDocumentFiles)
                .setSize(backupSize)
                .setTimeStart(startTime)
                .setTimeEnd(endTime)
                .build()
            storagePlugin.getBackupSnapshotOutputStream(startTime).use { outputStream ->
                outputStream.write(VERSION.toInt())
                val ad = streamCrypto.getAssociatedDataForSnapshot(startTime)
                streamCrypto.newEncryptingStream(streamKey, outputStream, ad)
                    .use { encryptingStream ->
                        backupSnapshot.writeTo(encryptingStream)
                    }
            }
        }
        val snapshotSize = backupSnapshot.serializedSize.toLong()
        val snapshotSizeStr = Formatter.formatShortFileSize(context, snapshotSize)
        Log.e(TAG, "Creating/writing snapshot took $snapshotWriteTime ($snapshotSizeStr)")

        val chunkIncrementTime = measureTime {
            db.applyInParts(result.chunkIds) {
                chunksCache.incrementRefCount(it)
            }
        }
        Log.e(TAG, "Incrementing chunk ref counters took $chunkIncrementTime")

        // updating the lastSeen time only here is an optimization (faster than file by file)
        // which we need to remember when purging old files from the cache
        val lastSeenUpdateTime = measureTime {
            val uris = filesResult.smallFiles.map { it.uri } + filesResult.files.map { it.uri }
            db.applyInParts(uris) {
                filesCache.updateLastSeen(it)
            }
        }
        Log.e(TAG, "Updating last seen time took $lastSeenUpdateTime")
    }

}
