package de.grobox.storagebackuptester

import android.app.Application
import android.net.Uri
import androidx.lifecycle.AndroidViewModel
import androidx.lifecycle.LiveData
import androidx.lifecycle.MutableLiveData
import androidx.lifecycle.asLiveData
import androidx.lifecycle.viewModelScope
import de.grobox.storagebackuptester.backup.BackupProgress
import de.grobox.storagebackuptester.backup.BackupStats
import de.grobox.storagebackuptester.content.BackupContentItem
import de.grobox.storagebackuptester.restore.RestoreProgress
import de.grobox.storagebackuptester.restore.RestoreStats
import de.grobox.storagebackuptester.scanner.scanTree
import de.grobox.storagebackuptester.scanner.scanUri
import kotlinx.coroutines.Dispatchers
import kotlinx.coroutines.flow.flowOn
import kotlinx.coroutines.launch
import kotlinx.coroutines.withContext
import org.calyxos.backup.storage.api.BackupContentType
import org.calyxos.backup.storage.api.EXTERNAL_STORAGE_PROVIDER_AUTHORITY
import org.calyxos.backup.storage.api.SnapshotItem
import org.calyxos.backup.storage.api.SnapshotResult
import org.calyxos.backup.storage.api.mediaItems
import org.calyxos.backup.storage.scanner.DocumentScanner
import org.calyxos.backup.storage.scanner.MediaScanner

private val logEmptyState = """
    Press the button below to simulate a backup. Your files won't be changed and not uploaded anywhere. This is just to test code for a future real backup.
    
    Please come back to this app from time to time and run a backup again to see if it correctly identifies files that were added/changed.
    
    Note that after updating this app, it might need to re-backup all files again.
    
    Thanks for testing!
""".trimIndent()

class MainViewModel(application: Application) : AndroidViewModel(application) {

    private val app: App = application as App
    private val settingsManager = app.settingsManager
    private val storageBackup = app.storageBackup

    private val _content = MutableLiveData<List<BackupContentItem>>()
    val content: LiveData<List<BackupContentItem>> = _content

    private val _backupLog = MutableLiveData(BackupProgress(0, 0, logEmptyState))
    val backupLog: LiveData<BackupProgress> = _backupLog

    private val _buttonEnabled = MutableLiveData<Boolean>()
    val backupButtonEnabled: LiveData<Boolean> = _buttonEnabled

    private val _restoreLog = MutableLiveData<RestoreProgress>()
    val restoreLog: LiveData<RestoreProgress> = _restoreLog

    private val _restoreProgressVisible = MutableLiveData<Boolean>()
    val restoreProgressVisible: LiveData<Boolean> = _restoreProgressVisible

    val snapshots: LiveData<SnapshotResult> = storageBackup.getBackupSnapshots()
        .flowOn(Dispatchers.IO)
        .asLiveData()

    init {
        viewModelScope.launch { loadContent() }
    }

    fun addUri(uri: Uri) {
        viewModelScope.launch {
            storageBackup.addUri(uri)
            loadContent()
        }
    }

    fun removeUri(uri: Uri) {
        viewModelScope.launch {
            storageBackup.removeUri(uri)
            loadContent()
        }
    }

    fun simulateBackup() {
        _buttonEnabled.value = false
        _backupLog.value = BackupProgress(0, 0, "")
        val backupObserver = BackupStats(app, _backupLog)
        viewModelScope.launch(Dispatchers.IO) {
            storageBackup.runBackup(backupObserver)
            _buttonEnabled.postValue(true)
        }
    }

    private suspend fun loadContent() = withContext(Dispatchers.Default) {
        val uris = storageBackup.uris
        val items = ArrayList<BackupContentItem>(uris.size)
        mediaItems.forEach { mediaType ->
            items.add(
                BackupContentItem(
                    mediaType.contentUri,
                    mediaType,
                    uris.contains(mediaType.contentUri)
                )
            )
        }
        uris.forEach { uri ->
            if (uri.authority == EXTERNAL_STORAGE_PROVIDER_AUTHORITY) {
                items.add(BackupContentItem(uri, BackupContentType.Custom, true))
            }
        }
        _content.postValue(items)
    }

    suspend fun scanMediaUri(uri: Uri): String = withContext(Dispatchers.Default) {
        scanUri(app, MediaScanner(app), uri)
    }

    suspend fun scanDocumentUri(uri: Uri): String = withContext(Dispatchers.Default) {
        scanTree(app, DocumentScanner(app), uri)
    }

    fun clearDb() {
        viewModelScope.launch(Dispatchers.IO) { storageBackup.clearCache() }
    }

    fun setBackupLocation(uri: Uri?) {
        if (uri != null) clearDb()
        settingsManager.setBackupLocation(uri)
    }

    fun hasBackupLocation(): Boolean {
        return settingsManager.getBackupLocation() != null
    }

    fun onSnapshotClicked(item: SnapshotItem) {
        val snapshot = item.snapshot
        check(snapshot != null)
        _restoreProgressVisible.value = true
        val restoreObserver = RestoreStats(app, _restoreLog)
        viewModelScope.launch(Dispatchers.IO) {
            storageBackup.restoreBackupSnapshot(snapshot, restoreObserver)
            _restoreProgressVisible.postValue(false)
        }
    }

}
