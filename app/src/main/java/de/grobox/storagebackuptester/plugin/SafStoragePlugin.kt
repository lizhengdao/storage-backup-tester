package de.grobox.storagebackuptester.plugin

import android.content.Context
import android.net.Uri
import android.util.Log
import androidx.documentfile.provider.DocumentFile
import de.grobox.storagebackuptester.crypto.KeyManager
import org.calyxos.backup.storage.api.StoragePlugin
import java.io.IOException
import java.io.InputStream
import java.io.OutputStream
import javax.crypto.SecretKey
import kotlin.time.ExperimentalTime
import kotlin.time.measureTime

private val chunkFolderRegex = Regex("[a-f0-9]{2}")
private val chunkRegex = Regex("[a-f0-9]{64}")
private val snapshotRegex = Regex("[0-9]{13}") // good until the year 2286
private const val CHUNK_FOLDER_COUNT = 256
private const val TAG = "SafStoragePlugin"

@Suppress("BlockingMethodInNonBlockingContext")
class SafStoragePlugin(
    private val context: Context,
    private val getLocationUri: () -> Uri?,
) : StoragePlugin {

    private val contentResolver = context.contentResolver

    private val nullStream = object : OutputStream() {
        override fun write(b: Int) {
            // oops
        }
    }
    private val chunkFolders = HashMap<String, DocumentFile>(CHUNK_FOLDER_COUNT)
    private val snapshotFiles = HashMap<Long, DocumentFile>()

    private fun getRoot(): DocumentFile? {
        val uri = getLocationUri() ?: return null
        return DocumentFile.fromTreeUri(context, uri) ?: error("No doc file from tree Uri")
    }

    @OptIn(ExperimentalTime::class)
    @Throws(IOException::class)
    override suspend fun getAvailableChunkIds(): List<String> {
        val root = getRoot() ?: return emptyList()
        val chunkIds = ArrayList<String>()
        populateChunkFolders(root) { file, name ->
            if (chunkFolderRegex.matches(name)) {
                chunkIds.addAll(getChunksFromFolder(file))
            }
        }
        Log.e(TAG, "Got ${chunkIds.size} available chunks")
        return chunkIds
    }

    @OptIn(ExperimentalTime::class)
    @Throws(IOException::class)
    private suspend fun populateChunkFolders(
        root: DocumentFile,
        fileOp: ((DocumentFile, String) -> Unit)? = null
    ) {
        val expectedChunkFolders = (0x00..0xff).map {
            Integer.toHexString(it).padStart(2, '0')
        }.toHashSet()
        val duration = measureTime {
            for (file in root.listFilesBlocking(context)) {
                val name = file.name ?: continue
                if (chunkFolderRegex.matches(name)) {
                    chunkFolders[name] = file
                    expectedChunkFolders.remove(name)
                }
                fileOp?.invoke(file, name)
            }
        }
        Log.e(TAG, "Retrieving chunk folders took $duration")
        createMissingChunkFolders(root, expectedChunkFolders)
    }

    @Throws(IOException::class)
    private fun getChunksFromFolder(chunkFolder: DocumentFile): List<String> {
        val chunkFiles = try {
            chunkFolder.listFiles()
        } catch (e: UnsupportedOperationException) {
            // can happen if this wasn't a directory after all
            throw IOException(e)
        }
        return chunkFiles.mapNotNull { chunkFile ->
            val name = chunkFile.name ?: return@mapNotNull null
            if (chunkRegex.matches(name)) name else null
        }
    }

    @OptIn(ExperimentalTime::class)
    @Throws(IOException::class)
    private fun createMissingChunkFolders(root: DocumentFile, expectedChunkFolders: Set<String>) {
        val s = expectedChunkFolders.size
        val duration = measureTime {
            for ((i, chunkFolderName) in expectedChunkFolders.withIndex()) {
                val file = root.createDirectoryOrThrow(chunkFolderName)
                chunkFolders[chunkFolderName] = file
                Log.d(TAG, "Created missing folder $chunkFolderName (${i + 1}/$s)")
            }
            if (chunkFolders.size != 256) throw IOException("Only have ${chunkFolders.size} chunk folders.")
        }
        if (s > 0) Log.e(TAG, "Creating $s missing chunk folders took $duration")
    }

    override fun getMasterKey(): SecretKey {
        return KeyManager.getMasterKey()
    }

    override fun hasMasterKey(): Boolean {
        return KeyManager.hasMasterKey()
    }

    @Throws(IOException::class)
    override fun getChunkOutputStream(chunkId: String): OutputStream {
        if (getLocationUri() == null) return nullStream

        val chunkFolderName = chunkId.substring(0, 2)
        val chunkFolder = chunkFolders[chunkFolderName] ?: error("No folder for chunk $chunkId")
        // TODO should we check if it exists first?
        val chunkFile = chunkFolder.createFile(chunkId)
        return chunkFile.getOutputStream(context.contentResolver)
    }

    override fun getBackupSnapshotOutputStream(timestamp: Long): OutputStream {
        val root = getRoot() ?: return nullStream
        val name = timestamp.toString()
        // TODO should we check if it exists first?
        val snapshotFile = root.createFile(name)
        return snapshotFile.getOutputStream(contentResolver)
    }

    /************************* Restore *******************************/

    @OptIn(ExperimentalTime::class)
    @Throws(IOException::class)
    override suspend fun getAvailableBackupSnapshots(): List<Long> {
        val root = getRoot() ?: return emptyList()
        val snapshots = ArrayList<Long>()

        populateChunkFolders(root) { file, name ->
            if (snapshotRegex.matches(name)) {
                val timestamp = name.toLong()
                snapshots.add(timestamp)
                snapshotFiles[timestamp] = file
            }
        }
        Log.e(TAG, "Got ${snapshots.size} snapshots while populating chunk folders")
        return snapshots
    }

    @Throws(IOException::class)
    override suspend fun getBackupSnapshotInputStream(timestamp: Long): InputStream {
        val snapshotFile = snapshotFiles.getOrElse(timestamp) {
            getRoot()?.findFileBlocking(context, timestamp.toString())
        } ?: throw IOException("Could not get file for snapshot $timestamp")
        return snapshotFile.getInputStream(contentResolver)
    }

    @Throws(IOException::class)
    override suspend fun getChunkInputStream(chunkId: String): InputStream {
        if (chunkFolders.size < CHUNK_FOLDER_COUNT) {
            val root = getRoot() ?: throw IOException("Could not get root")
            populateChunkFolders(root)
        }
        val chunkFolderName = chunkId.substring(0, 2)
        val chunkFolder = chunkFolders[chunkFolderName]
            ?: throw IOException("No folder for chunk $chunkId")
        val chunkFile = chunkFolder.findFileBlocking(context, chunkId)
            ?: throw IOException("No chunk $chunkId")
        return chunkFile.getInputStream(context.contentResolver)
    }

}

@Throws(IOException::class)
private fun DocumentFile.createDirectoryOrThrow(name: String): DocumentFile {
    val directory = createDirectory(name)
        ?: throw IOException("Unable to create directory: $name")
    if (directory.name != name) {
        directory.delete()
        throw IOException("Wanted to directory $name, but got ${directory.name}")
    }
    return directory
}

@Throws(IOException::class)
private fun DocumentFile.createFile(name: String): DocumentFile {
    val file = createFile(MIME_TYPE, name)
        ?: throw IOException("Unable to create file: $name")
    if (file.name != name) {
        file.delete()
        if (file.name == null) { // this happened what file existed already
            // try to find the original file we were looking for
            val foundFile = findFile(name)
            if (foundFile?.name == name) return foundFile
        }
        throw IOException("Wanted to create $name, but got ${file.name}")
    }
    return file
}
