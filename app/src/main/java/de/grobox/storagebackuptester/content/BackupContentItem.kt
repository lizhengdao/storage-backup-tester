package de.grobox.storagebackuptester.content

import android.content.Context
import android.net.Uri
import org.calyxos.backup.storage.api.BackupContentType
import org.calyxos.backup.storage.api.MediaType

data class BackupContentItem(
    val uri: Uri,
    val contentType: BackupContentType,
    val enabled: Boolean
) {
    fun getName(context: Context): String = when (contentType) {
        is BackupContentType.Custom -> BackupContentType.Custom.getName(uri)
        is MediaType -> context.getString(contentType.nameRes)
    }
}
