package de.grobox.storagebackuptester.settings

import android.content.Context
import android.content.Intent
import android.content.Intent.FLAG_GRANT_PERSISTABLE_URI_PERMISSION
import android.content.Intent.FLAG_GRANT_READ_URI_PERMISSION
import android.net.Uri
import android.os.Bundle
import android.provider.DocumentsContract
import android.util.Log
import android.view.LayoutInflater
import android.view.Menu
import android.view.MenuInflater
import android.view.MenuItem
import android.view.View
import android.view.ViewGroup
import android.widget.Toast
import android.widget.Toast.LENGTH_LONG
import android.widget.Toast.LENGTH_SHORT
import androidx.activity.result.contract.ActivityResultContracts.OpenDocumentTree
import androidx.appcompat.app.AlertDialog
import androidx.appcompat.widget.PopupMenu
import androidx.fragment.app.Fragment
import androidx.fragment.app.activityViewModels
import androidx.recyclerview.widget.RecyclerView
import com.google.android.material.floatingactionbutton.FloatingActionButton
import de.grobox.storagebackuptester.MainViewModel
import de.grobox.storagebackuptester.R
import de.grobox.storagebackuptester.content.BackupContentAdapter
import de.grobox.storagebackuptester.content.BackupContentItem
import de.grobox.storagebackuptester.restore.SnapshotFragment
import de.grobox.storagebackuptester.scanner.DocumentScanFragment
import de.grobox.storagebackuptester.scanner.MediaScanFragment
import org.calyxos.backup.storage.api.EXTERNAL_STORAGE_PROVIDER_AUTHORITY
import org.calyxos.backup.storage.api.MediaType


interface ContentClickListener {
    fun onContentClicked(item: BackupContentItem)
    fun onMediaContentEnabled(item: BackupContentItem, enabled: Boolean): Boolean
    fun onFolderOverflowClicked(view: View, item: BackupContentItem)
}

private class OpenTree : OpenDocumentTree() {
    override fun createIntent(context: Context, input: Uri?): Intent {
        return super.createIntent(context, input).apply {
            addFlags(FLAG_GRANT_READ_URI_PERMISSION or FLAG_GRANT_PERSISTABLE_URI_PERMISSION)
        }
    }
}

class SettingsFragment : Fragment(), ContentClickListener {

    companion object {
        fun newInstance() = SettingsFragment()
    }

    private val viewModel: MainViewModel by activityViewModels()
    private lateinit var backupLocationItem: MenuItem
    private lateinit var restoreItem: MenuItem

    private val addRequest = registerForActivityResult(OpenTree()) { uri ->
        onTreeUriReceived(uri)
    }
    private val backupLocationRequest = registerForActivityResult(OpenTree()) { uri ->
        onBackupUriReceived(uri)
    }

    override fun onCreateView(
        inflater: LayoutInflater,
        container: ViewGroup?,
        savedInstanceState: Bundle?
    ): View {
        setHasOptionsMenu(true)
        requireActivity().title = "Settings"
        val v = inflater.inflate(R.layout.fragment_content, container, false)
        val list: RecyclerView = v.findViewById(R.id.list)

        val adapter = BackupContentAdapter(this)
        list.adapter = adapter
        viewModel.content.observe(viewLifecycleOwner, {
            adapter.setItems(it)
        })

        v.findViewById<FloatingActionButton>(R.id.fab).setOnClickListener {
            addRequest.launch(DocumentsContract.buildRootsUri(EXTERNAL_STORAGE_PROVIDER_AUTHORITY))
        }

        return v
    }

    override fun onCreateOptionsMenu(menu: Menu, inflater: MenuInflater) {
        inflater.inflate(R.menu.fragment_settings, menu)
        backupLocationItem = menu.findItem(R.id.backup_location)
        backupLocationItem.isChecked = viewModel.hasBackupLocation()
        restoreItem = menu.findItem(R.id.restore_backup)
        restoreItem.isEnabled = backupLocationItem.isChecked
    }

    override fun onOptionsItemSelected(item: MenuItem): Boolean {
        return when (item.itemId) {
            R.id.info -> {
                parentFragmentManager.beginTransaction()
                    .replace(R.id.container, InfoFragment.newInstance("Media Info"))
                    .addToBackStack("INFO")
                    .commit()
                true
            }
            R.id.backup_location -> {
                if (item.isChecked) {
                    viewModel.setBackupLocation(null)
                    item.isChecked = false
                    restoreItem.isEnabled = false
                } else backupLocationRequest.launch(null)
                true
            }
            R.id.restore_backup -> {
                onRestoreClicked()
                true
            }
            R.id.clear_db -> {
                viewModel.clearDb()
                Toast.makeText(requireContext(), "Cache cleared", LENGTH_SHORT).show()
                true
            }
            else -> super.onOptionsItemSelected(item)
        }
    }

    override fun onContentClicked(item: BackupContentItem) {
        val f = if (item.contentType is MediaType) {
            MediaScanFragment.newInstance(item.getName(requireContext()), item.uri)
        } else {
            DocumentScanFragment.newInstance(item.getName(requireContext()), item.uri)
        }
        parentFragmentManager.beginTransaction()
            .replace(R.id.container, f)
            .addToBackStack("LOG")
            .commit()
    }

    override fun onMediaContentEnabled(item: BackupContentItem, enabled: Boolean): Boolean {
        if (enabled) {
            viewModel.addUri(item.uri)
        } else {
            viewModel.removeUri(item.uri)
        }
        return enabled
    }

    override fun onFolderOverflowClicked(view: View, item: BackupContentItem) {
        val popup = PopupMenu(requireContext(), view)
        popup.menuInflater.inflate(R.menu.item_custom, popup.menu)
        popup.setOnMenuItemClickListener {
            if (it.itemId == R.id.delete) {
                viewModel.removeUri(item.uri)
                true
            } else {
                false
            }
        }
        popup.show()
    }

    private fun onTreeUriReceived(uri: Uri?) {
        Log.e("TEST", "OpenDocumentTree uri: $uri")
        if (uri?.authority == EXTERNAL_STORAGE_PROVIDER_AUTHORITY) {
            requireContext().contentResolver.takePersistableUriPermission(
                uri, FLAG_GRANT_READ_URI_PERMISSION
            )
            viewModel.addUri(uri)
        } else {
            Toast.makeText(requireContext(), "Backup not supported for location", LENGTH_LONG)
                .show()
        }
    }

    private fun onBackupUriReceived(uri: Uri?) {
        if (uri == null) {
            Toast.makeText(requireContext(), "No location set", LENGTH_SHORT).show()
        } else {
            requireContext().contentResolver.takePersistableUriPermission(
                uri, FLAG_GRANT_READ_URI_PERMISSION
            )
            viewModel.setBackupLocation(uri)
            backupLocationItem.isChecked = true
            restoreItem.isEnabled = true
        }
    }

    private fun onRestoreClicked() {
        AlertDialog.Builder(requireContext())
            .setIcon(android.R.drawable.stat_sys_warning)
            .setTitle("Warning")
            .setMessage("This will override data and should only be used on a clean phone. Not the one you just made the backup on.")
            .setPositiveButton("I have been warned") { dialog, _ ->
                onStartRestore()
                dialog.dismiss()
            }
            .setNegativeButton(android.R.string.cancel) { dialog, _ ->
                dialog.dismiss()
            }
            .show()
    }

    private fun onStartRestore() {
        val f = SnapshotFragment.newInstance()
        parentFragmentManager.beginTransaction()
            .replace(R.id.container, f)
            .addToBackStack("SNAPSHOTS")
            .commit()
    }

}
