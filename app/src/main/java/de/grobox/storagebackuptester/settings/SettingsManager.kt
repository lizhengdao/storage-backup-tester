package de.grobox.storagebackuptester.settings

import android.content.Context
import android.net.Uri

private const val PREFS = "prefs"
private const val PREF_BACKUP_LOCATION = "backupLocationUri"

class SettingsManager(private val context: Context) {

    fun setBackupLocation(uri: Uri?) {
        context.getSharedPreferences(PREFS, 0)
            .edit()
            .putString(PREF_BACKUP_LOCATION, uri?.toString())
            .apply()
    }

    fun getBackupLocation(): Uri? {
        val uriStr = context.getSharedPreferences(PREFS, 0)
            .getString(PREF_BACKUP_LOCATION, null)
        return uriStr?.let { Uri.parse(it) }
    }

}
