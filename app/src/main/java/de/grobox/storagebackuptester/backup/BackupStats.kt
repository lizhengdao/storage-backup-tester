package de.grobox.storagebackuptester.backup

import android.content.Context
import android.provider.MediaStore
import android.text.format.DateUtils
import android.text.format.Formatter
import android.util.Log
import androidx.lifecycle.MutableLiveData
import org.calyxos.backup.storage.api.BackupFile
import org.calyxos.backup.storage.api.BackupObserver
import kotlin.time.DurationUnit
import kotlin.time.ExperimentalTime
import kotlin.time.toDuration

data class BackupProgress(
    val current: Int,
    val total: Int,
    val text: String?,
)

internal class BackupStats(
    private val context: Context,
    private val liveData: MutableLiveData<BackupProgress>,
) : BackupObserver {
    private var filesProcessed: Int = 0
    private var totalFiles: Int = 0
    private var filesUploaded: Int = 0
    private var expectedSize: Long = 0L
    private var size: Long = 0L
    private var savedChunks: Int = 0
    private val errorStrings = ArrayList<String>()

    override suspend fun onStart() {
    }

    override suspend fun onBackupStart(
        totalSize: Long,
        numFiles: Int,
        numSmallFiles: Int,
        numLargeFiles: Int
    ) {
        totalFiles = numFiles
        expectedSize = totalSize

        val totalSizeStr = Formatter.formatShortFileSize(context, totalSize)
        val text = "Backing up $totalFiles file(s) $totalSizeStr...\n" +
                "  ($numSmallFiles small, $numLargeFiles large)\n"
        liveData.postValue(BackupProgress(filesProcessed, totalFiles, text))
    }

    override suspend fun onFileBackedUp(
        file: BackupFile,
        wasUploaded: Boolean,
        reusedChunks: Int,
        bytesWritten: Long,
        tag: String,
    ) {
        filesProcessed++
        if (!wasUploaded) return

        savedChunks += reusedChunks
        filesUploaded++
        size += bytesWritten

        val sizeStr = Formatter.formatShortFileSize(context, file.size)
        val now = System.currentTimeMillis()
        val modStr = file.lastModified?.let {
            DateUtils.getRelativeTimeSpanString(it, now, 0L, DateUtils.FORMAT_ABBREV_ALL)
        } ?: "NULL"

        val volume =
            if (file.volume == MediaStore.VOLUME_EXTERNAL_PRIMARY) "" else "v: ${file.volume}"
        val text = "${file.path}\n   s: $sizeStr m: $modStr $volume"
        liveData.postValue(BackupProgress(filesProcessed, totalFiles, text))
    }

    override suspend fun onFileBackupError(file: BackupFile, tag: String) {
        filesProcessed++
        errorStrings.add("ERROR $tag: ${file.path}")
        liveData.postValue(BackupProgress(filesProcessed, totalFiles, null))
    }

    @OptIn(ExperimentalTime::class)
    override suspend fun onBackupComplete(backupDuration: Long) {
        val sb = StringBuilder("\n")
        errorStrings.forEach { sb.appendLine(it) }
        sb.appendLine()

        if (savedChunks > 0) sb.appendLine("Chunks re-used: $savedChunks")

        Log.e("TEST", "Total file size: $expectedSize")
        Log.e("TEST", "Actual size processed: $size")

        val sizeStr = Formatter.formatShortFileSize(context, size)
        val speed = getSpeed(size, backupDuration / 1000)

        val duration = backupDuration.toDuration(DurationUnit.MILLISECONDS)
        val perFile = if (filesUploaded > 0) duration.div(filesUploaded) else duration

        sb.appendLine("New/changed files backed up: $filesUploaded ($sizeStr)")
        if (filesUploaded > 0) sb.append("  ($perFile per file - ${speed}MB/sec)")
        liveData.postValue(BackupProgress(filesProcessed, totalFiles, sb.toString()))
    }

}

fun getSpeed(size: Long, duration: Long): Long {
    val mb = size / 1024 / 1024
    return if (duration == 0L) (mb.toDouble() / 0.01).toLong()
    else mb / duration
}
