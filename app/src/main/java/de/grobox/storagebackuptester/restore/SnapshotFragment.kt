package de.grobox.storagebackuptester.restore

import android.os.Bundle
import android.view.LayoutInflater
import android.view.View
import android.view.View.INVISIBLE
import android.view.ViewGroup
import android.widget.ProgressBar
import android.widget.Toast
import android.widget.Toast.LENGTH_LONG
import androidx.fragment.app.Fragment
import androidx.fragment.app.activityViewModels
import androidx.recyclerview.widget.RecyclerView
import de.grobox.storagebackuptester.MainViewModel
import de.grobox.storagebackuptester.R
import org.calyxos.backup.storage.api.SnapshotItem
import org.calyxos.backup.storage.api.SnapshotResult

interface SnapshotClickListener {
    fun onSnapshotClicked(item: SnapshotItem)
}

class SnapshotFragment : Fragment(), SnapshotClickListener {

    companion object {
        fun newInstance(): SnapshotFragment = SnapshotFragment()
    }

    private val viewModel: MainViewModel by activityViewModels()

    override fun onCreateView(
        inflater: LayoutInflater,
        container: ViewGroup?,
        savedInstanceState: Bundle?
    ): View {
        requireActivity().title = "Available backups"
        val v = inflater.inflate(R.layout.fragment_snapshot, container, false)
        val list: RecyclerView = v.findViewById(R.id.list)

        val progressBar: ProgressBar = v.findViewById(R.id.progressBar)

        val adapter = SnapshotAdapter(this)
        list.adapter = adapter
        viewModel.snapshots.observe(viewLifecycleOwner, {
            progressBar.visibility = INVISIBLE
            when (it) {
                is SnapshotResult.Success -> adapter.setItems(it.snapshots)
                is SnapshotResult.Error -> {
                    Toast.makeText(requireContext(), "Error loading snapshots", LENGTH_LONG).show()
                }
            }
        })

        return v
    }

    override fun onSnapshotClicked(item: SnapshotItem) {
        viewModel.onSnapshotClicked(item)
        val f = RestoreFragment.newInstance()
        parentFragmentManager.beginTransaction()
            .replace(R.id.container, f)
            .addToBackStack("RESTORE")
            .commit()
    }

}
